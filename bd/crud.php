<?php

include_once "../bd/conexion.php";
$objeto = new Conexion();
$conexion = $objeto -> Conectar();

// Recepcion de Datos
$id = (isset($_POST['id'])) ? $_POST['id'] : '';
$nombre = (isset($_POST['nombre'])) ? $_POST['nombre'] : '';
$usuario = (isset($_POST['usuario'])) ? $_POST['usuario'] : '';
$rol = (isset($_POST['rol'])) ? $_POST['rol'] : '';
$estatus = (isset($_POST['estatus'])) ? $_POST['estatus'] : '';
$clave = (isset($_POST['clave'])) ? $_POST['clave'] : '';
$opcion = (isset($_POST['opcion'])) ? $_POST['opcion'] : '';

switch ($opcion) {
    // Registrar
    case 1:
        $consulta = sprintf("INSERT INTO personas (nombre, usuario, rol, estatus, clave) VALUES('%s', '%s', '%s', '%s', '%s')", $nombre, $usuario, $rol, $estatus, $clave);			
        $resultado = $conexion -> prepare($consulta);   
        $resultado -> execute(); 

        $consulta = "SELECT * FROM personas ORDER BY id DESC LIMIT 1";
        $resultado = $conexion -> prepare($consulta);
        $resultado -> execute();
        $data = $resultado -> fetchAll(PDO::FETCH_ASSOC);
        break;
    // Modificar
    case 2:
        $consulta = sprintf("UPDATE personas SET nombre='%s', usuario='%s', rol='%s', estatus='%s', clave='%s' WHERE id='%s'", $nombre, $usuario, $rol, $estatus, $clave, $id);		
        $resultado = $conexion -> prepare($consulta);
        $resultado -> execute();        
        
        $consulta = "SELECT * FROM personas WHERE id='$id'";       
        $resultado = $conexion -> prepare($consulta);
        $resultado -> execute();
        $data = $resultado -> fetchAll(PDO::FETCH_ASSOC);
        break;
    // Eliminar
    case 3:
        $consulta = "DELETE FROM personas WHERE id='$id'";		
        $resultado = $conexion->prepare($consulta);
        $resultado -> execute();
        
        $consulta = "SELECT * FROM personas ORDER BY id DESC LIMIT 1";       
        $resultado = $conexion -> prepare($consulta);
        $resultado -> execute();
        $data = $resultado -> fetchAll(PDO::FETCH_ASSOC);
        break; 
}


//enviar el array final en formato json a JS
print json_encode($data, JSON_UNESCAPED_UNICODE); 
$conexion = NULL;


?>