"use strict";

$(document).ready(() => {

    tablaPersonas = $("#tablaPersonas").DataTable({
        "columnDefs":[
            {"targets": -1, "data":null, "className": "dt-center", "defaultContent": "<div class='text-center'><button class='btn btn-primary mr-2 btnModificar'>Editar</button><button class='btn btn-danger btnEliminar'>Borrar</button></div></div>"},
            {"targets": [0, 1, 2, 3, 4], "className": "text-center"}
 
     ],
        
    //Para cambiar el lenguaje a español
    "language": {
            "lengthMenu": "Mostrar _MENU_ registros",
            "zeroRecords": "No se encontraron resultados",
            "info": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "infoEmpty": "Mostrando registros del 0 al 0 de un total de 0 registros",
            "infoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sSearch": "Buscar:",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast":"Último",
                "sNext":"Siguiente",
                "sPrevious": "Anterior"
             },
             "sProcessing":"Procesando...",
        }
    });
    
    let id,
        opcion,
        fila;

    $("#btnNuevo").click(() => {
        $("#formPersonas").trigger("reset");
        $(".modal-header").css("background-color", "#28a745");
        $(".modal-header").css("color", "#ffffff");
        $(".modal-title").text("Nuevo Usuario");            
        $("#modalCRUD").modal("show");

        opcion = 1;
    });


    $(document).on("click", ".btnModificar", function() {
        fila = $(this).closest("tr");
        id = parseInt(fila.find('td:eq(0)').text());
        let nombre = fila.find('td:eq(1)').text(),
            usuario = fila.find('td:eq(2)').text(),
            rol = fila.find('td:eq(3)').text(),
            estatus = fila.find('td:eq(4)').text();
        
        $("#nombre").val(nombre);
        $("#usuario").val(usuario);
        $("#rol").val(rol); 
        $("#estatus").val(estatus);
        $("#password").val("");

        $(".modal-header").css("background-color", "#007bff");
        $(".modal-header").css("color", "#ffffff");
        $(".modal-title").text("Editar Usuario");
        $("#password").attr("placeholder", "Definir Nueva Contraseña");       
        $("#modalCRUD").modal("show");  

        opcion = 2;
    });


    $(document).on("click", ".btnEliminar", function() {
        fila = $(this);
        id = parseInt($(this).closest("tr").find('td:eq(0)').text());

        let respuesta = confirm(`¿Está seguro de eliminar el registro con id ${id}?`);

        opcion = 3;

        if (respuesta) {
            $.ajax({
                url: "bd/crud.php",
                type: "POST",
                dataType: "json",
                data: {
                    opcion, id
                },
                success: () => {
                    tablaPersonas.row(fila.parents('tr')).remove().draw();
                }
            });
        }   
    });


    $("#formPersonas").submit((event) => {
        event.preventDefault();    
        let nombre = $.trim($("#nombre").val()),
            usuario = $.trim($("#usuario").val()),
            rol = $.trim($("#rol").val()),
            estatus = $.trim($("#estatus").val()),
            clave = $.trim($("#password").val());

        $.ajax({
            url: "bd/crud.php",
            type: "POST",
            dataType: "JSON",
            data: {
                id, nombre, usuario, rol, estatus, clave, opcion
            },
            success: (data) => {
                console.log(data);
                id = data[0].id;            
                nombre = data[0].nombre;
                usuario = data[0].usuario;
                rol = data[0].rol;
                estatus = data[0].estatus;
                clave = data[0].clave;
                
                if (opcion === 1) {
                    tablaPersonas.row.add([id, nombre, usuario, rol, estatus]).draw();
                } else {
                    tablaPersonas.row(fila).data([id,nombre, usuario, rol, estatus]).draw();
                }  
            }
        });
        $("#modalCRUD").modal("hide");   
    });
});